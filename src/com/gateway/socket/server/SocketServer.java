package com.gateway.socket.server;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

public class SocketServer {
	private byte[] buffer = new byte[2048];
	
	InputStream reader = null;
	OutputStream writer = null;
	private Socket client;

	private static boolean isWritterRunning;
	
	static ArrayList<SocketServer> connectedClients = new ArrayList<SocketServer>();
	static ArrayList<byte[]> messagesQueue = new ArrayList<byte[]>();
	static ReentrantLock lockMessagesRegister = new ReentrantLock();
	static ReentrantLock lockClientRegister = new ReentrantLock();
	
	public void Start(ServerSocket socket) throws Exception {
		client = socket.accept();
		System.out.println("Connection from client accepted - " + 
				client.getInetAddress().getHostAddress());
		
		try {
			reader = client.getInputStream();
			writer = client.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
			client.close();
			return;
		}

		registerClient(this);

		startWriterThread();
		startReaderThread();
	}

	/*
	 * Method receive messages from the connected client socket
	 */
	private void ReceiveMessage() throws Exception{
		if(reader == null)
			return;
		
		int bytesRead = 0;	
		int totalBytesRead = 0;
		int length = 0;
		try{
			//Amount of bytes to read
			byte[] dataLength = new byte[4];
			bytesRead = reader.read(dataLength);
			length = ByteBuffer.wrap(dataLength).getInt();
			System.out.println("Data length: " + length);
			
			if(bytesRead == -1){
				System.out.println("The stream is over");
				throw new Exception();
			}
			//Receive data
			buffer = new byte[length];
			do{
				bytesRead = reader.read(buffer, totalBytesRead, buffer.length - totalBytesRead);
				totalBytesRead += bytesRead;
			}while(totalBytesRead < length && bytesRead != -1 && bytesRead != 0);
			
		}catch (Exception e) {			
			System.out.println("unexpected data - read = " + totalBytesRead);
			System.out.println("unexpected data - expected = " + length);
			throw e;
		}
		
		try{
			lockMessagesRegister.lock();
			//Add space for a 11 bytes header "length:'\o'" + int32 representing the message lenght
			byte[] data = new byte[length + 11];
			
			byte[] field = new String("length:"+'\0').getBytes(StandardCharsets.UTF_8);
			System.arraycopy(field, 0, data, 0, field.length);
			
			byte[] size = ByteBuffer.allocate(4).putInt(length).array();
			System.arraycopy(size, 0, data, field.length - 1, size.length);
			System.arraycopy(buffer, 0, data, field.length + size.length - 1, length);
			
			System.out.println("Queued message: '" + new String(data, 0, data.length >= 15 ? 15 : data.length) + "...'");
			messagesQueue.add(data);
		}finally{
			lockMessagesRegister.unlock();
		}
	}
	
	/*
	 * Method broadcast all received messages to the connected client sockets
	 */
	private static void SendMessage() throws Exception{
		SocketServer s = null;
		try{
			lockMessagesRegister.lock();
			//Send data
			for (int i=0; i<connectedClients.size(); i++) {
				s = connectedClients.get(i);
				for (byte[] b : messagesQueue) {
					s.writer.write(b);
					System.out.println("Message '" + new String(b, 0, 10) + "...' sent to client " + s.client.getInetAddress().getHostAddress());
				}
			}
			messagesQueue.clear();
		}catch(Exception e){
			e.printStackTrace();
			if(s != null){
				unregisterClient(s);
				s.client.close();
			}
			throw e;
		}finally{
			lockMessagesRegister.unlock();
		}
	}
	
	private static void registerClient(SocketServer socketServer) {
		try{
			lockClientRegister.lock();
			connectedClients.add(socketServer);
		}finally{
			lockClientRegister.unlock();
		}
	}
	
	private static void unregisterClient(SocketServer socketServer) {
		try{
			lockClientRegister.lock();
			connectedClients.remove(socketServer);
		}finally{
			lockClientRegister.unlock();
		}
	}
	
	private static void startWriterThread() {
		if(isWritterRunning)
			return;
		
		isWritterRunning = true;
		new Thread(new Runnable(){
			@Override
			public void run() {				
				while(true){
					try {
						SendMessage();
					} catch (Exception e) {
						e.printStackTrace();
						isWritterRunning = false;
						break;
					}
				}		
			}
		}).start();
	}
	
	private void startReaderThread() {
		new Thread(new Runnable(){
			@Override
			public void run() {				
				while(true){
					try {
						ReceiveMessage();
					} catch (Exception e) {
						e.printStackTrace();
						break;
					}
				}		
				try {
					unregisterClient(SocketServer.this);
					System.out.println("Connection to client " + client.getInetAddress().getHostAddress() + " closed.");
					client.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	
	public static void main(String[] args) throws IOException {
		System.out.println("Valor da porta para receber conex�es: ");
		Scanner in = new Scanner(System.in);
		int port = in.nextInt();
		in.close();
		ServerSocket socket = new ServerSocket(port);
		
		while(true){
			try {
				new SocketServer().Start(socket);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
}


