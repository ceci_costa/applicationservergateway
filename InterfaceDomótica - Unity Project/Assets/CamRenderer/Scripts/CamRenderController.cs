﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(WebCamTextureController))]
public class CamRenderController : Singleton<CamRenderController> {
    
    public UnityEngine.UI.RawImage renderImage;
    WebCamTextureController webCamTextureController;

    public bool flipHorizontal;
    public bool flipVertical;

    private Quaternion baseRotation;
    private Vector3 baseScale;

    public UnityEventBytes onUpdateFrame;

    private float captureWidth;
    private float captureHeight;

    private void Awake() {
        Screen.fullScreen = false;
        StartCoroutine(UpdateFrame());
    }
    
    void Start() {
        baseRotation = transform.rotation;
        baseScale = renderImage.transform.localScale;

        webCamTextureController = gameObject.GetComponent<WebCamTextureController>();
        webCamTextureController.Init();
    }

    public void OnWebCamTextureControllerInited() {
        WebCamTexture webCamTexture = webCamTextureController.GetWebCamTexture();

        ResizeAndKeepAspect(renderImage, webCamTexture.width / webCamTexture.height);            
        renderImage.texture = webCamTexture;
        Flip(); 
    }
        
    void Update() {
        WebCamTexture webCamTexture = webCamTextureController.GetWebCamTexture();
        renderImage.transform.rotation = baseRotation * Quaternion.AngleAxis(-webCamTexture.videoRotationAngle, Vector3.forward);
    }

    void OnDisable () {
        webCamTextureController.Dispose ();
	}

	public void OnPlayButton () {
		webCamTextureController.Play ();
	}

	public void OnPauseButton () {
		webCamTextureController.Pause ();
	}

	public void OnStopButton () {
		webCamTextureController.Stop ();
	}

	public void OnChangeCameraButton () {
		webCamTextureController.Init (null, webCamTextureController.requestWidth, webCamTextureController.requestHeight, !webCamTextureController.requestIsFrontFacing);
	}

	public Rect GetSize(){
		return new Rect (0, 0, webCamTextureController.requestWidth, webCamTextureController.requestHeight);
	}

    private void Flip() {
        renderImage.transform.localScale = baseScale;

        WebCamTexture webCamTexture = webCamTextureController.GetWebCamTexture();
        int flipCode = int.MinValue;

        if (webCamTextureController.GetWebCamDevice().isFrontFacing) {
            if (webCamTexture.videoRotationAngle == 0) {
                flipCode = 1;
            } else if (webCamTexture.videoRotationAngle == 90) {
                flipCode = 1;
            }
            if (webCamTexture.videoRotationAngle == 180) {
                flipCode = 0;
            } else if (webCamTexture.videoRotationAngle == 270) {
                flipCode = 0;
            }
        } else {
            if (webCamTexture.videoRotationAngle == 180) {
                flipCode = -1;
            } else if (webCamTexture.videoRotationAngle == 270) {
                flipCode = -1;
            }
        }

        if (flipVertical) {
            if (flipCode == int.MinValue) {
                flipCode = 0;
            } else if (flipCode == 0) {
                flipCode = int.MinValue;
            } else if (flipCode == 1) {
                flipCode = -1;
            } else if (flipCode == -1) {
                flipCode = 1;
            }
        }

        if (flipHorizontal) {
            if (flipCode == int.MinValue) {
                flipCode = 1;
            } else if (flipCode == 0) {
                flipCode = -1;
            } else if (flipCode == 1) {
                flipCode = int.MinValue;
            } else if (flipCode == -1) {
                flipCode = 0;
            }
        }

        if (flipCode > int.MinValue) {
            Vector2 s = renderImage.transform.localScale;
            renderImage.transform.localScale = flipCode == 0? new Vector3(s.x, s.y * -1): flipCode == 1? new Vector3(s.x * -1, s.y) : new Vector3(s.x* -1, s.y* -1);
        }
    }

    public void ResizeAndKeepAspect(RawImage image, float aspect, float scaleFactor = 1) {
        RectTransform layout =  image.GetComponent<RectTransform>();
        if (layout != null) {
            Vector2 size = layout.rect.size;
            size.x = Screen.width / 2 - 5;
            size.y = size.x / aspect;
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x / scaleFactor);
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y / scaleFactor);
        }
    }

    IEnumerator UpdateFrame() {
        yield return new WaitUntil(() => webCamTextureController != null && webCamTextureController.IsInited());

        while (webCamTextureController.IsInited()) {
            yield return new WaitForEndOfFrame();
            
            //float aspect = (float)captureWidth / (float)captureHeight;
            //float areaW = (float)captureWidth / 2f;
            //float areaH = (float)areaW / aspect;
            //float y = (captureHeight - areaH) / 2f;
            //Rect rect = new Rect(0, y, areaW, areaH);

            Texture2D snap = new Texture2D(webCamTextureController.GetWebCamTexture().width, webCamTextureController.GetWebCamTexture().height);
            snap.SetPixels(webCamTextureController.GetWebCamTexture().GetPixels());
            snap.Apply();

            byte[] bytes = snap.EncodeToPNG();

            onUpdateFrame.Invoke(bytes);
            DestroyImmediate(snap);

            //RenderTexture renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
            //Texture2D tex = new Texture2D((int)areaW, captureHeight, TextureFormat.RGB24, false);

            //foreach (Camera c in cameras) {

            //    RenderTexture rt = c.targetTexture;
            //    c.targetTexture = renderTexture;
            //    c.Render();

            //    RenderTexture.active = renderTexture;
            //    tex.ReadPixels(rect, 0, 0);
            //    tex.Apply();
            //    c.targetTexture = rt;
            //    RenderTexture.active = null;
            //}

            //// Encode texture into PNG
            //byte[] bytes = tex.EncodeToJPG();

            //Graphics.Blit(tex, renderToTexture);
            //DestroyImmediate(renderTexture);
            //DestroyImmediate(tex);
        }
    }

}

