﻿using UnityEngine.Events;
[System.Serializable]
public class UnityEventBytes : UnityEvent<byte[]> { }

