﻿using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class UnityEventTexture : UnityEvent<Texture> { }
