﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;


public class WebCamTextureController : MonoBehaviour
{

    public string requestDeviceName = null;

    public int requestWidth = 640;
    public int requestHeight = 480;

    public bool requestIsFrontFacing = false;
    public bool flipVertical = false;
    public bool flipHorizontal = false;

    public int timeoutFrameCount = 300;

    public UnityEvent OnInitedEvent;
    public UnityEvent OnDisposedEvent;

    public ErrorUnityEvent OnErrorOccurredEvent;

    WebCamTexture webCamTexture;
    WebCamDevice webCamDevice;

    bool initWaiting = false;
    bool initDone = false;

    ScreenOrientation screenOrientation = ScreenOrientation.Unknown;

    [System.Serializable]
    public enum ErrorCode : int{
        CAMERA_DEVICE_NOT_EXIST = 0,
        TIMEOUT = 1,
    }

    [System.Serializable]
    public class ErrorUnityEvent : UnityEngine.Events.UnityEvent<ErrorCode>{}

    void Update ()
    {
        if (initDone) {
            if (screenOrientation != Screen.orientation) {
                StartCoroutine (init ());
            }
        }
    }
    
    public void Init () {
        if (initWaiting)
            return;

        if (OnInitedEvent == null)
            OnInitedEvent = new UnityEvent ();
        if (OnDisposedEvent == null)
            OnDisposedEvent = new UnityEvent ();
        if (OnErrorOccurredEvent == null)
            OnErrorOccurredEvent = new ErrorUnityEvent ();

        StartCoroutine (init ());
    }

    /// <summary>
    /// Init this instance.
    /// </summary>
    /// <param name="deviceName">Device name.</param>
    /// <param name="requestWidth">Request width.</param>
    /// <param name="requestHeight">Request height.</param>
    /// <param name="requestIsFrontFacing">If set to <c>true</c> request is front facing.</param>
    /// <param name="OnInited">On inited.</param>
    public void Init (string deviceName, int requestWidth, int requestHeight, bool requestIsFrontFacing) {
        if (initWaiting)
            return;

        this.requestDeviceName = deviceName;
        this.requestWidth = requestWidth;
        this.requestHeight = requestHeight;
        this.requestIsFrontFacing = requestIsFrontFacing;
        if (OnInitedEvent == null)
            OnInitedEvent = new UnityEvent ();
        if (OnDisposedEvent == null)
            OnDisposedEvent = new UnityEvent ();
        if (OnErrorOccurredEvent == null)
            OnErrorOccurredEvent = new ErrorUnityEvent ();

        StartCoroutine (init ());
    }

    /// <summary>
    /// Init this instance by coroutine.
    /// </summary>
    private IEnumerator init () {
        if (initDone)
            Dispose ();

        initWaiting = true;

        if (!String.IsNullOrEmpty (requestDeviceName)) {
            webCamTexture = new WebCamTexture (requestDeviceName, requestWidth, requestHeight);
        } else {
            // Checks how many and which cameras are available on the device
            for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++) {
                Debug.Log(WebCamTexture.devices[cameraIndex].name);
                if (WebCamTexture.devices [cameraIndex].isFrontFacing == requestIsFrontFacing) {

                    //Debug.Log (cameraIndex + " name " + WebCamTexture.devices [cameraIndex].name + " isFrontFacing " + WebCamTexture.devices [cameraIndex].isFrontFacing);
                    webCamDevice = WebCamTexture.devices [cameraIndex];
                    webCamTexture = new WebCamTexture (webCamDevice.name, requestWidth, requestHeight);

                    break;
                }
            }
        }

        if (webCamTexture == null) {
            if (WebCamTexture.devices.Length > 0) {
                webCamDevice = WebCamTexture.devices [0];
                webCamTexture = new WebCamTexture (webCamDevice.name, requestWidth, requestHeight);
            } else {
                //Debug.Log("Camera device does not exist.");
                initWaiting = false;

                if (OnErrorOccurredEvent != null)
                    OnErrorOccurredEvent.Invoke (ErrorCode.CAMERA_DEVICE_NOT_EXIST);
                yield break;
            }
        }

        //Debug.Log ("name " + webCamTexture.name + " width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);

        // Starts the camera
        webCamTexture.Play ();

        int initCount = 0;
        bool isTimeout = false;

        while (true) {
            if (initCount > timeoutFrameCount) {
                isTimeout = true;
                break;
            } else if (webCamTexture.didUpdateThisFrame) {
                //Debug.Log ("Screen.orientation " + Screen.orientation);
                screenOrientation = Screen.orientation;
                initWaiting = false;
                initDone = true;

                if (OnInitedEvent != null)
                    OnInitedEvent.Invoke ();

                break;
            } else {
                initCount++;
                yield return 0;
            }
        }

        if (isTimeout) {
            //Debug.Log("Init time out.");
            webCamTexture.Stop ();
            webCamTexture = null;
            initWaiting = false;

            if (OnErrorOccurredEvent != null)
                OnErrorOccurredEvent.Invoke (ErrorCode.TIMEOUT);
        }
    }

    public bool IsInited () {
        return initDone;
    }

    public void Play () {
        if (initDone)
            webCamTexture.Play ();
    }

    public void Pause () {
        if (initDone)
            webCamTexture.Pause ();
    }

    public void Stop () {
        if (initDone)
            webCamTexture.Stop ();
    }

    public bool IsPlaying () {
        if (!initDone)
            return false;
        return webCamTexture.isPlaying;
    }

    public WebCamTexture GetWebCamTexture () {
        return (initDone) ? webCamTexture : null;
    }

    public WebCamDevice GetWebCamDevice () {
        return webCamDevice;
    }

    public bool DidUpdateThisFrame () {
        if (!initDone)
            return false;
        return webCamTexture.didUpdateThisFrame;
    }

    public void Dispose () {
        if (!initDone)
            return;
        initWaiting = false;
        initDone = false;

        if (webCamTexture != null) {
            webCamTexture.Stop ();
            webCamTexture = null;
        }
           
        if (OnDisposedEvent != null)
            OnDisposedEvent.Invoke ();
    }
}
