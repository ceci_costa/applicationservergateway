﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public partial class ImageTransmissor : MonoBehaviour {

	public string field;

    public UnityEventBytes updateFrame;
	public void OnUpdateData(byte[] bytes) {
        byte[] header = Encoding.UTF8.GetBytes(field + ":" + '\0');
        byte[] buffer = new byte[bytes.Length + header.Length];

        Array.Copy(header, 0, buffer, 0, header.Length);
        Array.Copy(bytes, 0, buffer, header.Length-1, bytes.Length);

        SocketController.Instance.SendData(buffer);
        updateFrame.Invoke(buffer);
    }
    
}
