﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text;

[RequireComponent(typeof(RawImage))]
public class ImageReceiver : MonoBehaviour {

    //Evento disparado quando uma mensagem é recebida
    public UnityEventTexture updateImage;

    private Texture2D texture;
    public string field;

    object locker = new object();
    private void Start() {
        //Define método de callback quando uma mensagem é recebida
        SocketController.Instance.onDataReceived += OnDataReceived;
        texture = new Texture2D(1, 1);
    }

    public void OnDataReceived(byte[] data) {

        //The name of the object is expected to be encoded in the first 'field+":"' bytes, for the Camera
        byte[] header = Encoding.UTF8.GetBytes(field + ":");
        if (Encoding.UTF8.GetString(data, 0, header.Length).CompareTo(field+":") == 0) {
            lock(locker){
                DestroyImmediate(texture);
                texture = new Texture2D(1, 1);
                texture.LoadImage(data.Skip(header.Length).ToArray<byte>());
                //Resize the image to adapt to the aspect ratio of the texture
                AdjustImageAspectRatio(GetComponent<RawImage>(), texture.width, texture.height);
            }
        }
    }

    private void Update() {
        lock (locker) {
            updateImage.Invoke(texture);
        }
    }

    public void AdjustImageAspectRatio(RawImage image, float width, float height, float scaleFactor = 1) {
        RectTransform layout = image.GetComponent<RectTransform>();
        float aspect = width / height;
        if (layout != null) {
            Vector2 size = layout.rect.size;
            size.x = Screen.width / 2 - 5;
            size.y = size.x / aspect;
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x / scaleFactor);
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y / scaleFactor);
        }
    }
}
