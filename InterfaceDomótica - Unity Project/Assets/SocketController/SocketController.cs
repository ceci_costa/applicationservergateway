﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System;
using System.Text;
using System.IO;
using System.Linq;

public class SocketController : Singleton<SocketController> {

    public string serverAddress = "192.168.1.2";
    public string port = "10400";

    private TcpClient socketConnection;
    private Thread receiverThread;
    private Thread startThread;
    private Thread senderThread;
    private bool sendOperationPending = false;

    private object lockBuffer = new object();
    private object assyncLock = new object();
    private List<byte[]> queuedMessages = new List<byte[]>();
    private byte[] buffer = new byte[] { };

    public delegate void OnDataReceived(byte[] byffer);
    public OnDataReceived onDataReceived;

    public delegate void OnMessageReceived(string message);
    public OnMessageReceived onMessageReceived;
    private bool running = false;
    private bool started = false;
    private bool connectionLost;

    public string ServerAddress {
        get {
            return serverAddress;
        }

        set {
            serverAddress = value;
        }
    }

    public string Port {
        get {
            return port;
        }

        set {
            port = value;
        }
    }

    public void Connect() {
        startThread = new Thread(() => {
            StartConnection();
        });
        startThread.Start();
    }

    public void StartConnection() {
        if (socketConnection == null || !socketConnection.Connected) {
            try {
                Debug.Log("Attempt to Connect to server: " + serverAddress + ":" + port);
                socketConnection = new TcpClient(serverAddress, int.Parse(port));
                Debug.Log("Connected");
                StartTransferProcesses();
            }
            catch (SocketException e) {
                Debug.Log("Socket exception: " + e);
            }
        }
    }

    private void StartTransferProcesses() {
        running = true;
        senderThread = new Thread(() => {
            DataSender();
        });
        senderThread.Start();

        receiverThread = new Thread(() => {
            DataReceiver();
        });
        receiverThread.Start();
        started = true;
    }

    private void CloseSocket() {
        running = false;

        if (startThread != null && startThread.IsAlive) {
            startThread.Abort();
            Debug.Log("StartThread closed");
        }

        if (senderThread != null && senderThread.IsAlive) {
            Debug.Log("StartThread closed");
            senderThread.Abort();
        }

        if (receiverThread != null && receiverThread.IsAlive) {
            Debug.Log("StartThread closed");
            receiverThread.Abort();
        }

        if (socketConnection != null) {
            try {
                Debug.Log("Closing connection");
                socketConnection.GetStream().Close();
                socketConnection.Close();
                Debug.Log("Connection closed");
            }
            catch (Exception e) {
                Debug.LogError(e);
            }
        }
        Debug.Log("Connection Lost");
    }

    public void SendData(string data) {
        if (sendOperationPending)
            return;
        // Convert string message to byte array.   
        buffer = Encoding.UTF8.GetBytes(data);
        sendOperationPending = true;
    }
    public void SendData(byte[] data) {
        if (sendOperationPending)
            return;
        buffer = new byte[data.Length];
        Array.Copy(data, buffer, data.Length);
        sendOperationPending = true;
    }

    void DataSender() {
        Debug.Log("Data sender Started");
        
        while (running) {
            try {
                if (socketConnection == null || !socketConnection.Connected)
                    continue;
                if (!sendOperationPending)
                    continue;
                
                NetworkStream stream = socketConnection.GetStream();
                stream.WriteTimeout = 1000;
                if (stream.CanWrite) {
                    //Send data size
                    byte[] size = BitConverter.GetBytes(buffer.Length);
                    stream.Write(size.Reverse().ToArray(), 0, 4);

                    //Write data.
                    stream.Write(buffer, 0, buffer.Length);
                    Debug.Log("Data send to server - " + buffer.Length + " bytes sent");
                }
                sendOperationPending = false;
            }

            catch (Exception e) {
                Debug.Log("Socket exception: " + e);
            }
        }

        connectionLost = true;
    }

    void DataReceiver() {
        Debug.Log("Data receiver Started");
       
        try {
            while (running) {
                if (socketConnection == null || !socketConnection.Connected)
                    continue;

                int totalBytesRead = 0;
                // Get a stream object for writing. 			
                NetworkStream stream = socketConnection.GetStream();
                if (stream.CanRead && stream.DataAvailable) {

                    //Size of "length:" + int32
                    byte[] data = new byte[11];
                    //Extract the header string "lenght:"
                    int bytesRead = stream.Read(data, 0, data.Length);
                    string header = Encoding.UTF8.GetString(data, 0, 7);

                    if (!header.Contains("length:")) {
                        Debug.Log("Flushing not recognized data");
                        stream.Flush();
                        continue;
                    }

                    //Extract the length from the next 4 bytes
                    byte[] dataLength = new byte[4];
                    Array.Copy(data, 7, dataLength, 0, 4);
                    Int32 length = BitConverter.ToInt32(dataLength.Reverse<byte>().ToArray(), 0);

                    data = new byte[length];
                    do {
                        bytesRead = stream.Read(data, totalBytesRead, length - totalBytesRead);
                        totalBytesRead += bytesRead;
                    } while (totalBytesRead < length && bytesRead != -1 && bytesRead != 0);
                    Debug.Log(totalBytesRead);

                    Debug.Log("Data Received");
                    //Queue the new message
                    lock (assyncLock) {
                        queuedMessages.Add(data);
                    }

                    //Debug.Log("Data received from server - " + totalBytesRead + " bytes received");
                }
            }
        }
        catch (Exception e ) {
            Debug.Log("Socket exception: " + e);
        }

        connectionLost = true;
    }

    private void Update() {
        if (!started)
            return;

        if (connectionLost) {
            CloseSocket();
            Connect();
            connectionLost = false;
        }

        lock (assyncLock) {
            foreach (byte[] b in queuedMessages) {
                if (onMessageReceived != null)
                    onMessageReceived(Encoding.UTF8.GetString(b, 0, b.Length));
                if (onDataReceived != null)
                    onDataReceived(b);
            }
            queuedMessages.Clear();
        }
    }

    public bool IsSendFinished() {
        return !sendOperationPending;
    }

    private byte[] MultBytesArrayToSingle(List<byte[]> bytesArrayList) {
        byte[] output = new byte[bytesArrayList.Sum(arr => arr.Length)];
        using (var stream = new MemoryStream(output))
            foreach (var bytes in bytesArrayList)
                stream.Write(bytes, 0, bytes.Length);
        return output;
    }

    private void OnApplicationQuit() {
        CloseSocket();
    }
}