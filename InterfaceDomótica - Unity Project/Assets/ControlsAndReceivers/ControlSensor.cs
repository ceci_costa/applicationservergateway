﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class ControlSensor : MonoBehaviour {

	public string field;

	public void OnControlChange(string value) {
        //Envia informação dos sensores
		SocketController.Instance.SendData(field + ":" + value);
    }
}
