﻿using UnityEngine;

public class ReceiverBool: MonoBehaviour {
    //Evento disparado quando uma mensagem é recebida
    public UnityEventBool updateState;

    private bool stateValue;
	public string field;

    private void Start() {
        //Define método de callback quando uma mensagem é recebida
        SocketController.Instance.onMessageReceived += OnMessageReceived;
    }

    public void OnMessageReceived(string message) {
        
        if (message.Contains(field) && (message.Contains("on")))
            stateValue = true;

		if (message.Contains(field) && (message.Contains("off")))
            stateValue = false;
    }

    private void Update() {
        updateState.Invoke(stateValue);
    }
}
