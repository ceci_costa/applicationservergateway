﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Control : MonoBehaviour {

	public string field;

    public void OnControlChange(bool on) {
        //Envia o comando da referente ao controle on/off"
        SocketController.Instance.SendData(field + ":" + (on? "on" : "off"));
    }
}
