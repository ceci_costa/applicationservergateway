﻿using UnityEngine;

public class ReceiverString: MonoBehaviour {
    //Evento disparado quando uma mensagem é recebida
    public UnityEventString updateField;

    private string fieldValue = "";
	public string field;

    private void Start() {
        //Define método de callback quando uma mensagem é recebida
        SocketController.Instance.onMessageReceived += OnMessageReceived;
    }

    public void OnMessageReceived(string message) {
       
        if (message.Contains(field))
			fieldValue = message.Substring(message.IndexOf(':') + 1);
    }

    private void Update() {
        updateField.Invoke(fieldValue);
    }
}
